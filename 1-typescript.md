
---

TeamEverywhere Typescript Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [정의](#정의?)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 정의
  - Typescript는 마이크로소포트에서 개발, 유지중인 오픈소스 프로그래밍 언어 입니다.
  - Typescript로 프로그래밍을 하면 자바스크립트로 컴파일되어 실행됩니다.
  - 타입이 없어 고통받는 Javascript 업계를 위해 만들어졌습니다.
  - 환경설정 : npm install -g typescript
  - 컴파일 : tsc 파일.ts
  - 실행 : javascript 파일 실행


----
