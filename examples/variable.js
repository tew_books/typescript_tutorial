class Car {
    constructor() {
        this._window = 4;
    }
    move() {
        console.log('move!');
    }
    get windows() {
        return this._window;
    }
    set windows(value) {
        this._window = value;
    }
}
let bmw = new Car();
console.log(bmw.windows);
