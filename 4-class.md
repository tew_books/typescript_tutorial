
---

TeamEverywhere Typescript Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Class Members
  ```typescript   
    class Point {
      x: number;
      y: number;
    }
    
    const pt = new Point();
    pt.x = 0;
    pt.y = 0;
  ```
  - 멤버변수 초기화
  ```typescript   
    class Point {
      x = 0;
      y = 0;
    }
    
    const pt = new Point();
    // Prints 0, 0
    console.log(`${pt.x}, ${pt.y}`);
  ```
  - 생성자 활용
  ```typescript   
    class Greeter {
      name: string;
    
      constructor() {
        this.name = "hello";
      }
    }
    const greeter = new Greeter()
    console.log(greeter.name)

    class Greeter2 {
      name: string;
    
      constructor(name:string) {
        this.name = name;
      }
    }
    const greeter = new Greeter2("how are you?")
    console.log(greeter.name)
  ```
  - typescript는 멤버변수의 readonly 설정이 가능합니다.
  ```typescript   
    class Greeter {
      readonly name: string = "world";    
      constructor(otherName?: string) {
        if (otherName !== undefined) {
          this.name = otherName;
        }
      }
    }
    const greeter = new Greeter();
    greeter.name = "new name"; 
  ```
## super call
  ```typescript   
      class Car {
        window = 4;
      }
      
      class BMW extends Car {
        constructor() {
          super();
          console.log(this.window);    
        }
      }

      let bmw = new BMW()
      console.log(bmw.window)
  ```

## Method
  ```typescript   
     class Car {
      window = 4;
      move():void{
        console.log('move!')
      }
    }
    let bmw = new Car()
    bmw.move()
  ```

## Getter/Setter
  - 실행 : tsc variable.ts --target ES2016
  - get은 있고 set은 없다면 해당 property는 자동으로 readonly가 됩니다.
  - setter의 파라미터가 정해져있지 않다면 get의 리턴 타입을 참조합니다.
  ```typescript   
     class Car {
      _window:number = 4;
      
      move():void{
        console.log('move!')
      }
      get windows() {
        return this._window;
      }
      set windows(value) {
        this._window = value;
      }
    }
    let bmw = new Car()
    bmw.windows = 5
    console.log(bmw.windows)
    
  ```

## 상속
  ```typescript   
     interface Pingable {
      ping(): void;
    }
    
    class Sonar implements Pingable {
      ping() {
        console.log("ping!");
      }
    }
  ```
  - 클래스는 여러개의 interface를 상속받을 수 있습니다.
  ```typescript   
      interface Car1 {
        move(): void;
      }
      interface Car2 {
        stop(): void;
      }

      class BMW implements Car1, Car2 {
        move() {
          console.log("move!");
        }
        stop() {
          console.log("stop!");
        }
      }

      const bmw = new BMW()
      bmw.stop()    
  ```

  ## Member Visibility
    - public
    - 실행 : tsc variable.ts --target ES2016
    ```typescript   
      class Car {
        public _window:number = 4;
        
        move():void{
          console.log('move!')
        }
        get windows() {
          return this._window;
        }
        set windows(value) {
          this._window = value;
        }
      }
      let bmw = new Car()
      bmw._window = 5  
      console.log(bmw._window)
    ```
    - protected
    - 실행 : tsc variable.ts --target ES2016
    ```typescript   
      class Car {
        protected _window:number = 4;
        
        move():void{
          console.log('move!')
        }
        get windows() {
          return this._window;
        }
        set windows(value) {
          this._window = value;
        }
      }
      let bmw = new Car()
      bmw._window = 5  
      console.log(bmw._window)
    ```

    - private
    - 실행 : tsc variable.ts --target ES2016
    ```typescript   
      class Car {
        private _window:number = 4;
        
        move():void{
          console.log('move!')
        }
        get windows() {
          return this._window;
        }
        set windows(value) {
          this._window = value;
        }
      }
      let bmw = new Car()
      bmw._window = 5  
      console.log(bmw._window)
    ```

    - private과 protected
    - protected는 확장된 클래스에서 참조가 가능합니다.
    ```typescript   
        class Car {
          private _window:number = 4
          protected _door:number = 4          
        }
        class BMW extends Car {
          change(){
            this._window = 5
            this._door = 5
          }
        }
    ```