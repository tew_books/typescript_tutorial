
---

TeamEverywhere Typescript Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## String
  - 문자열로도 알려진 일련의 텍스트. 그 값이 문자열이라는 것을 나타내기 위해서는 인용부호로 둘러싸야 합니다.
  ```typescript
    let myName:string = "dahan"    
  ```

## Number  
  - 숫자를 나타낸다.  
  ```typescript
    let three:number = 3    
  ```

## Array  
  - 배열
  ```typescript
    let num_list:number[] = [1, 2, 3]
  ```

## any  
  - Typescript에는 any라는 특별한 타입이 있습니다.
  - 특정 타입을 원하지 않을때 타입 체킹 에러를 피하고자 사용할 수 있습니다.
  - 단 이것은 당신이 타입스크립트보다 환경을 더 안다고 간주하는 것이니 함부로 사용하지 맙시다.
  - with great power comes great responsibility...
  ```typescript
    let obj: any = { name:'wow' };
    obj.method();
    obj();
    obj.number = 100;
    obj = "hello";
    const n: number = obj;
  ```

## Type Annotations   
  - string, number 등은 선언 시 타입을 정해주지 않아도 값을 통해 인식합니다.
  ```typescript
    let myName = "dahan"    
  ```



## Parameter Type Annotations
  - 파라미터의 타입을 맞추어야 합니다.
  ```typescript
    function greet(name: string) {
      console.log("Hello, " + name);
    }
    greet(42);
  ```

## Return Type Annotations
  - 리턴 타입을 지정할 수 있습니다.
  ```typescript
    function getString(): string {
      return 'wow';
    }
  ```

## Anonymous Functions
  - 익명함수 사용시, 파라미터의 타입이 자동으로 주어집니다.
  - 아래와 같은 경우 배열의 아이템 s의 파라미터가 반복문 내에서 주어지지 않았지만 Typescript가 처리해주었습니다.
  ```typescript
    // No type annotations here, but TypeScript can spot the bug
    const names = ["one", "two", "three"];
    
    // Contextual typing for function
    names.forEach(function (s) {
      console.log(s.toUpperCase());    
    });
    
    // Contextual typing also applies to arrow functions
    names.forEach((s) => {
      console.log(s.toUpperCase());    
    });
  ```

## Object Types
  - Object 타입 선언시, ';'로 구분하여 객체 내 값(property)들을 정할 수 있습니다.
  ```typescript
    function addLocation(pt: { x: number; y: number }) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
    }
    addLocation({ x: 3, y: 7 });
  ```

## Optional Properties
  - Property는 옵셔널로 선언할 수 있습니다.
  - 값이 전달되지 않은 옵셔널 property를 읽을때는 undefined가 호출 됩니다.
  ```typescript
    function addLocation(pt: { x: number; y?: number }) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
    }
    addLocation({ x: 3});
  ```

## Union Types
  - Union Type은 여러개의 타입을 property로 가져갈 수 있는 형태입니다.
  ```typescript
    function myNickName(nickname: number | string) {
      console.log("Your NickName is: " + nickname);
    }
    // OK
    myNickName(1232);
    // OK
    myNickName("wow");
    // Error
    myNickName({ nickname: 22342 });
  ```
  - Union Type의 함수 활용시 자료형 메서드를 호출할 때는 주의 해야 합니다.
  ```typescript
    function myNickName(nickname: number | string) {
      console.log("Your NickName is: " + nickname.toLowerCase());
    }    

    // 아래와 같이 되어야 합니다.
    function myNickName(nickname: number | string) {
       if (typeof nickname === "string") {
        console.log(nickname.toLowerCase());
      } else {
        console.log(nickname);
      }
    }
  ```  

## Type Aliases (별칭)
  - Type을 직접 선언해서 사용할 수 있습니다!
  ```typescript
    type Point = {
      x: number;
      y: number;
    };
    function addLocation(pt: Point) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
    }
    addLocation({ x: 3});
  ```
 
## Interfaces
  - object type을 선언할 수 있는 또 다른 방법입니다.
  ```typescript
    interface Point {
      x: number;
      y: number;
    }
    
    function addLocation(pt: Point) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
    }
    addLocation({ x: 3, y:10});
  ```
## Type과 Interface의 차이
 - Type은 property를 추가 하기 위해 다시 선언할 수 없습니다.
  ```typescript
    interface Point {
      x: number;
      y: number;
    }
    interface Point {
      z: number;
    }
    function addLocation(pt: Point) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
      console.log("The location's z value is " + pt.z);
    }
    addLocation({ x: 3, y:10, z:30});
  ```

  ```typescript
    type Point = {
      x: number;
      y: number;
    };
    type Point = {
      z: number;
    };
    function addLocation(pt: Point) {
      console.log("The location's x value is " + pt.x);
      console.log("The location's y value is " + pt.y);
      console.log("The location's z value is " + pt.z);
    }
    addLocation({ x: 3, y:10, z:30});
  ```
 - 상속의 방법이 다릅니다.
  ```typescript
    interface Car {
      window: number
    }
    interface BMW extends Car {
      door: number
    }
    const car = {window:3}
    const bmw = {window:3, door:5}
  ```
  ```typescript
    type Car = {
      window: number
    };
    type BMW = Car & {
      door: number
    };
    const car = {window:3}
    const bmw = {window:3, door:5}
  ```

## enum
  - enum은 Typescript에서 몇 안되는 javascript의 type-level 확장 내용이 아닙니다.
    (결론적으로 tsc로 컴파일 되는 요소에서 제외 된다는 의미 입니다.)
  - 아래와 같은 경우 auto-increment가 적용됩니다.
  ```typescript
    enum Direction {
      Up = 1,
      Down,
      Left,
      Right,
    }
  ```
  - 문자열로도 선언이 가능합니다.
  ```typescript
    enum Direction {
      Up = "UP",
      Down = "DOWN",
      Left = "LEFT",
      Right = "RIGHT",
    }
  ```
  - string과 number를 섞을 수 있습니다.
  ```typescript
    enum MixEnum {
      No = 0,
      Yes = "YES",
    }
  ```