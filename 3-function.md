
---

TeamEverywhere Typescript Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Function Type Expressions
  - 함수 타입을 지정할 수 있습니다.  
  ```typescript   
    // 하나의 파라미터를 가진 함수라는 의미의 타입
    function hello( fn: (a: string) => void) {
      fn("Hello, World");
    }
    
    function log(s: string) {
      console.log(s);
    }
    
    hello(log);
  ```
  - type으로 함수의 타입을 만들 수 있습니다.
  ```typescript
    type callFunction = (a: string) => void;
    function hello(fn: callFunction) {
      // ...
    } 
  ```
## Call Signatures
 - Call Signature 기능을 활용하여 type내에 호출 가능한 proporty 작성이 가능합니다.
   ```typescript   
    // 하나의 파라미터를 가진 함수라는 의미의 타입
    function hello( fn: (a: string) => void) {
      fn("Hello, World");
    }
    
    function log(s: string) {
      console.log(s);
    }
    
    hello(log);
  ```
  - type으로 함수의 타입을 만들 수 있습니다.
  ```typescript
    type callSignatureFunc = {
      desc: string;
      num: number;
    };
    function doSomething(fn: callSignatureFunc) {
      console.log(fn.desc + " returned " + fn.num);
    }
    doSomething({desc:'test',num:3})
  ```

## Construct Signatures
 - new 연산자를 활용해 object를 생성할 수 있습니다.
  ```typescript
    class SomeObject {
      constructor (public s: string) {
        console.log('ctor invoked : ', s);
      }
    }
    type ConstructorObject = {
      new (s: string): SomeObject;
    };
    function fn(ctor: ConstructorObject) {
      return new ctor("hello");
    }
    fn(SomeObject)
  ```

